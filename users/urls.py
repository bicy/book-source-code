from django.urls import path
from .views import (
    SignUpView,
    UserDeleteView,
    UserUpdateView,
    UserDetailView,
    UserListView,
    UserUpdateView,
    UserCreateView
)

urlpatterns = [
    path('signup/',SignUpView.as_view(), name='signup'), 
    path("",UserListView.as_view(), name="user_list"),
    path("<int:pk>/edit/",UserUpdateView.as_view(),name="user_edit"),
    path("<int:pk>/",UserDetailView.as_view(),name="user_detail"),
    path("<int:pk>/delete/",UserDeleteView.as_view(),name="user_delete"),
    path("new/", UserCreateView.as_view(), name="user_new")

]