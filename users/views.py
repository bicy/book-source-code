from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from .models import CustomUser
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

# Create your views here.
from .form import CustomUserCreationForm


class UserRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required = ("users.delete_user","users.change_user")


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

class UserCreateView(UserRequiredMixin,CreateView):
    model = CustomUser
    form_class = CustomUserCreationForm
    template_name = "users/user_new.html"

class UserListView(UserRequiredMixin,ListView):
    model = CustomUser
    template_name = "users/user_list.html"

class UserDetailView(UserRequiredMixin,DetailView):
    model = CustomUser
    template_name = "users/user_detail.html"

class UserUpdateView(UserRequiredMixin,UpdateView):
    model = CustomUser
    template_name = "users/user_edit.html"
    fields = ("username", "email", "first_name", "last_name", "user_permissions", "groups", "is_staff","is_customer","is_moderator")

class UserDeleteView(UserRequiredMixin,DeleteView):
    model = CustomUser
    template_name = "users/user_delete.html"
    success_url = reverse_lazy("user_list")

