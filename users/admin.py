from csv import list_dialects
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .form import CustomerUserChangeForm, CustomUserCreationForm
from .models import CustomUser
# Register your models here.


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomerUserChangeForm
    model = CustomUser
    list_display = ('email','is_staff','is_active',)
    fieldsets = (
        (('Personal info'), {'fields': ('email','username','password')}),
        (('Roles'), {'fields': ('is_active', 'is_staff')}),
        (('Permissions'),{'fields':('user_permissions',)}),
        (('Groups'), {'fields':('groups',)}),
    )
    add_fields = (
        (None, {
        'classes':('wide',),
        'fields':('email','password1','password2','is_staff','is_active')}),
    )
    ordering = ('email',)
    exclude = ()
   
admin.site.register(CustomUser, CustomUserAdmin)
