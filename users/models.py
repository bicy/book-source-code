from pyexpat import model
from django.urls import reverse
from django.db import models
from django.contrib.auth.models import Group
from django.utils import timezone
from django.contrib.auth.models import (AbstractUser, BaseUserManager)


class MyUserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        """
         creates and saves a User with the given email and password
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        creates and saves a superuser with the given, email and password
        """
        extra_fields .setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser',True)
        extra_fields.setdefault('is_active', True)

        return self.create_user(email, password, **extra_fields)




# Create your models here.
class CustomUser(AbstractUser):
    email = models.EmailField(verbose_name='email adress',unique=True)
    username = models.CharField(("username"),max_length=80,default="")
    
   
    objects = MyUserManager()


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "users"
        verbose_name_plural = "users"

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return reverse("user_detail", kwargs={"pk": self.pk})
    
