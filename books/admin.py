from django.contrib import admin
from .models import Book
# Register your models here.

class BookAdmin(admin.ModelAdmin):
    model = Book
    exclude = ('hit_count_generic',)

admin.site.register(Book, BookAdmin)
