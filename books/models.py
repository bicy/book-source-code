from pyexpat import model
from tabnanny import verbose
from django.db import models
from django.urls import reverse

from hitcount.models import HitCountMixin, HitCount
from authors.models import Author
from publishers.models import Publisher
from categories.models import Category


# Create your models here.
class Book (models.Model, HitCountMixin):

    title = models.CharField(max_length=50, verbose_name='book title', unique=True)
    isbn = models.CharField(max_length=13, verbose_name='book isbn', unique=True)
    total_page = models.IntegerField()
    category = models.ManyToManyField(Category)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    author = models.ManyToManyField(Author)
    image = models.ImageField(upload_to="images/books",blank=True)
    description  = models.TextField(blank=True)

    hit_count_generic = models.ForeignKey(HitCount,on_delete=models.CASCADE,
                                   related_query_name='hit_count_generic_relation',null=True)


    class Meta:
        verbose_name = "book"
        verbose_name_plural = ("Books")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("book_detail", kwargs={"pk": self.pk})


    

    