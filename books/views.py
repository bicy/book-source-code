from re import template
from django.contrib.auth.mixins import (LoginRequiredMixin,PermissionRequiredMixin)
from django.contrib.auth import get_user_model
from django.shortcuts import render, get_list_or_404
from django.urls import reverse_lazy
from django.views.generic import (ListView,UpdateView, CreateView, DetailView, DeleteView)

from .models import Book
from reviews.models import Review
from comments.models import Comment
from hitcount.views import HitCountDetailView


# Book generic view class 
class BookPermissionRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = 'login'
    permission_required = ('books.add_book','books.change_book','books.delete_book')

class BookCreateView(BookPermissionRequiredMixin,CreateView):
    model = Book
    template_name = 'books/book_new.html'
    fields = ("title", 'total_page', 'category', 'publisher','author','image','description')


class BookListView(BookPermissionRequiredMixin,ListView):
    model = Book
    template_name = 'books/book_list.html'
  
    
class BookDetailView(HitCountDetailView):
    model = Book
    template_name = "books/book_detail.html"
    count_hit = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        book = self.get_object()
        comments = Comment.objects.filter(book__pk = book.pk)
        context["comments"] = comments
        context['page_title'] = "Book detail"
        context["count"] = comments.count()
    
        has_review = False
        reviews = Review.objects.filter(book__pk=book.pk)
        context['review_list'] = reviews
       
        if reviews.count() > 0:
            has_review = True

        context['has_review'] = has_review
        return context

class BookDeleteView(BookPermissionRequiredMixin,DeleteView):
    model = Book
    template_name = "books/book_delete.html"
    success_url = reverse_lazy("books")
  
class BookUpdateView(BookPermissionRequiredMixin,UpdateView):
    model = Book
    fields = "__all__"
    template_name = "books/book_edit.html"


    

