from django.shortcuts import render
from django.contrib.auth.mixins import (LoginRequiredMixin, UserPassesTestMixin)

from django.views.generic import TemplateView, ListView, DetailView
from django.db.models import Q

from books.models import Book
from authors.models import Author
# Create your views here.


class HomePageView(TemplateView):
    template_name = 'home.html'
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Boogle"
        return context


class AccountPageView(LoginRequiredMixin, TemplateView):
    template_name = 'account.html'
    login_url = 'login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "settings"
        return context



