from django.shortcuts import render, get_object_or_404, redirect, resolve_url
from django.contrib.auth.mixins import (PermissionRequiredMixin, LoginRequiredMixin)
from django.urls import reverse_lazy
from django.contrib import messages
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView, DetailView)
# Create your views here.

from .models import Comment
from .forms import AddCommentForm
from books.models import Book

class CommentRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required = ("comments.delete_comment", "comments.change_comment")


class CommentCreateView(CommentRequiredMixin, CreateView):
    model = Comment
    template_name = "comments/comment_new.html"
    fields = "__all__"

class CommentListView(CommentRequiredMixin, ListView):
    model = Comment
    template_name = "commments/comment_list.html"

class CommentUpdateView(CommentRequiredMixin, UpdateView):
    model = Comment
    template_name = "comments/comment_edit.html"
    fields = "__all__"

class CommentDeleteView(CommentRequiredMixin, DeleteView):
    model = Comment
    template_name = "comments/comment_delete.html"
    success_url = reverse_lazy("comment_list")

class CommentDetailView(CommentRequiredMixin,DetailView):
    model = Comment
    template_name = "comments/comment_detail.html"
    

def add_comment_view(request):
    template_name = "books/book_detail.html"
    context = {}
    if request.method == 'POST':
        postdata = request.POST.copy()
        content = postdata.get('content')
        book_id = postdata.get("pk")

        book = get_object_or_404(Book, pk=book_id)
        comments = Comment.objects.filter(book__pk = book_id)
        context['book'] = book 
        context['comments'] = comments
        if not request.user.is_authenticated:
            messages.warning(request,"login before adding comment")
            return redirect('book_detail',book_id)
        
        user = request.user
        Comment.objects.create(book=book,user=user,content=content)

    return render(request,template_name,context)