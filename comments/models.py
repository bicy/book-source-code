from django.db import models
from django.urls import reverse

from users.models import CustomUser
from books.models import Book

# Create your models here.


class Comment(models.Model):
    content = models.TextField(("commnet content"))
    created_at = models.DateField(auto_now_add=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'comment'
        verbose_name_plural = 'comments'


    def get_absolute_url(self):
        return reverse("comment_detail", kwargs={"pk": self.pk})
