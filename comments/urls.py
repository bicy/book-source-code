from django.urls import path

from .views import (
    CommentCreateView,
    CommentDeleteView,
    CommentDetailView,
    CommentListView,
    CommentUpdateView,
    add_comment_view
)

urlpatterns = [
     
     path('', CommentListView.as_view(), name="comment_list"),
     path("new/", CommentCreateView.as_view(), name="comment_new"),
     path("<int:pk>/edit/", CommentUpdateView.as_view(), name="comment_edit"),
     path("<int:pk>/delete/",CommentDeleteView.as_view(), name="comment_delete"),
     path("<int:pk>/", CommentDetailView.as_view(), name="comment_detail"),
     path("add/",add_comment_view, name="add_comment")
]
