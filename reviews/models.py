from django.db import models
from django.urls import reverse

from books.models import Book
from users.models import CustomUser

# Create your models here.

class Review(models.Model):

    review_title = models.CharField(("review title"), max_length=80, unique=True)
    review_body = models.TextField(("review content"), blank=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    is_approved = models.BooleanField(default=False)


    class Meta:
        verbose_name = 'review'
        verbose_name_plural = ("Reviews")

    def __str__(self) -> str:
        return self.review_title

    def get_absolute_url(self):
        return reverse("review_detail", kwargs={"pk": self.pk})