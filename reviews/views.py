from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (ListView, CreateView, UpdateView, DetailView, DeleteView)
from django.contrib.auth.mixins import (LoginRequiredMixin, PermissionRequiredMixin)

from .models import Review
# Create your views here.

class ReviewRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required  = ("reviews.delete_review","reviews.edit_review")

class ReviewCreateView(ReviewRequiredMixin,CreateView):
    model = Review
    template_name  = "reviews/review_new.html"
    fields = "__all__"

class ReviewListView(ListView):
    model = Review
    template_name = "reviews/review_list.html"

class ReviewDetailView(DetailView):
    model = Review
    template_name = "reviews/review_detail.html"
    
class ReviewUpdateView(UpdateView):
    model = Review
    template_name = "reviews/review_edit.html"
    fields = "__all__"

class ReviewDeleteView(DeleteView):
    model = Review
    template_name = "reviews/review_delete.html"
    success_url = reverse_lazy("review_list")