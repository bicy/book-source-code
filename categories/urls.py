from django.urls import path

from .views import  (
    CategoryListView,
    CategoryDetailView,
    CategoryUpdateView,
    CategoryDeleteView,
    CategoryCreateView,
    book_by_category_view)

urlpatterns = [
    
    path('',CategoryListView.as_view(), name="category_list"),
    path('new/',CategoryCreateView.as_view(), name='category_new'),
    path('<int:pk>/', CategoryDetailView.as_view(), name='category_detail'),
    path('<int:pk>/edit/', CategoryUpdateView.as_view(), name="category_edit"),
    path('<int:pk>/delete/',CategoryDeleteView.as_view(),name="category_delete"),
    path('<int:pk>/books/', book_by_category_view, name='category_book'),
]
