from django.shortcuts import render, get_list_or_404
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic import (ListView, UpdateView, CreateView, DetailView, DeleteView)
from django.urls import reverse_lazy

from .models import Category
from books.models import Book

# category  Mixin class for Authorization
class CategoryPermissionRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required = ('categories.add_category', 'categories.delete_category', 'categories.change_category')

  
    
# Create your views here.
class CategoryCreateView(CategoryPermissionRequiredMixin, CreateView):
    model = Category
    fields = "__all__"
    template_name = "categories/category_new.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "add category" 
        return context

class CategoryListView(CategoryPermissionRequiredMixin,ListView):
    model = Category
    template_name = "categories/category_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "all categories" 
        return context

class CategoryDetailView(CategoryPermissionRequiredMixin,DetailView):
    model = Category
    template_name = "categories/category_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "category detail" 
        return context

class CategoryUpdateView(CategoryPermissionRequiredMixin,UpdateView):
    model = Category
    template_name = "categories/category_edit.html"
    fields = "__all__"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "update category" 
        return context

class CategoryDeleteView(CategoryPermissionRequiredMixin,DeleteView):
    model = Category 
    template_name = "categories/category_delete.html"
    success_url = reverse_lazy('category_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "delete category" 
        return context

    
def book_by_category_view(request, pk):
    context = {}
    template_name = 'categories/category_book.html'

    category = Category.objects.get(pk=pk)
    book_list_by_category = Book.objects.filter(category=category)
    
    context['book_category_list'] = book_list_by_category
    context['category'] = category
    return render(request, template_name, context)