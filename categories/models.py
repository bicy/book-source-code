from django.db import models
from django.urls import reverse

# Create your models here.

class Category(models.Model):
    """
       This class is used to set book category
    """
    category_name = models.CharField(("category name"), max_length=80)

    class Meta:
        verbose_name = "category"
        verbose_name_plural = ("categories")

    def __str__(self):
        return self.category_name

    def get_absolute_url(self):
        return reverse("category_detail", kwargs={"pk": self.pk})