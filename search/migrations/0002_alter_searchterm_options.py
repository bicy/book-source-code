# Generated by Django 4.0.3 on 2022-03-27 07:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='searchterm',
            options={'verbose_name': 'search term', 'verbose_name_plural': 'search terms'},
        ),
    ]
