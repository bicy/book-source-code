from django.urls import path

from .views import SearchResultView, SearchBookDetailView, SearchAuthorDetailtView

urlpatterns = [
    
    path('',SearchResultView.as_view(), name="search_results"),
    path("results/book/<int:pk>/",SearchBookDetailView.as_view(), name="book_results" ),
    path("results/author/<int:pk>/",SearchAuthorDetailtView.as_view(), name="search_author_detail")
]
