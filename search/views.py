import imp
from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.views.generic import ListView, DetailView
from books.models import Book
from authors.models import Author
from comments.models import Comment

from bookapp_project import settings
from . import search

# Create your views here.
class SearchResultView(ListView):
    model = Book
    template_name = "search_results.html"

        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
    
        q = self.request.GET.get('q')
        if len(q) == 0:
            return context
        

        matching = search.books(q).get('books').distinct()
        sorted_books = sorted(matching, key=lambda instance:instance.hit_count.hits, reverse=True)
        paginator = Paginator(matching, settings.BOOKS_PER_PAGE)

        # store the search
        search.store(self.request, q)
        page = self.request.GET.get('page')

        context['page_title'] = "Search results for: " + q

        books = paginator.get_page(page)
        nums = "a" * books.paginator.num_pages
        context['book_list'] = sorted_books
        context["count"] = matching.count()
        context['nums'] = nums
        context['q'] = q
        return context

class SearchBookDetailView(DetailView):
    model = Book
    template_name = "searches/search_book_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        book = self.get_object()
        comments = Comment.objects.filter(book__pk = book.pk)
        context["comments"] = comments
        context['page_title'] = "Book detail"
        context["count"] = comments.count()
        return context

class SearchAuthorDetailtView(DetailView):
    model = Author
    template_name = "searches/search_author_detail.html"