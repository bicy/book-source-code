from tabnanny import verbose
from django.db import models

from users.models import CustomUser
# Create your models here.

class SearchTerm(models.Model):
    q = models.CharField(max_length=50)
    search_date = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField()
    user = models.ForeignKey(CustomUser, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.q

    class Meta:
        verbose_name = "search term"
        verbose_name_plural = "search terms"

    
