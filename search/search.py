from django.db.models import Q

from .models import SearchTerm
from books.models import Book

STRIP_WORDS = ['a','and','an','by','for','from','in','no','not','of',
                'on','or','that','this','the','to','with']

# store the search text in the database
def store(request, q):
    # if search term is at least three chars long, store in db
    if len(q) > 2:
        term = SearchTerm()
        term.q = q 
        term.ip_address = request.META.get("REMOTE_ADDR")
        term.user = None
        if request.user.is_authenticated:
            term.user = request.user
        term.save()

# get books matching the search text
def books(search_text):
    words = _prepare_words(search_text)
    books = Book.objects.all()
    results = {}
    results['books'] = []
    # iterate througth keywords
    for word in words:
        books = books.filter(Q(title__icontains=word) | Q(category__category_name__icontains=word))
        results['books'] = books
    return results
    
# strip out common words, limit to 5 words
def _prepare_words(search_text):
    words = search_text.split()
    for common in STRIP_WORDS:
        if common in words:
            words.remove(common)
    return words[0:5]
