from django.urls import path

from .views import (
    AuthorCreateView, 
    AuthorUpdateView, 
    AuthorDeleteView,
    AuthorListView, 
    AuthorDetailView,
    book_by_author_view)

urlpatterns = [ 

    path('', AuthorListView.as_view(), name="author_list"),
    path('<int:pk>/', AuthorDetailView.as_view(), name="author_detail"),
    path("<int:pk>/edit/", AuthorUpdateView.as_view(), name="author_edit"),
    path("<int:pk>/delete/", AuthorDeleteView.as_view(), name="author_delete"),
    path('new/', AuthorCreateView.as_view(), name="author_new"),
    path('<int:pk>/books/', book_by_author_view, name="author_book"),
]