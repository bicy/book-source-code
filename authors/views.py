from re import template
from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (ListView, UpdateView, DetailView, DeleteView, DetailView, CreateView)
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin

from .models import Author
from books.models import Book

class AuthorRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required = ("authors.add_author","authors.delete_author","authors.change_author")


# Create your views here.
class AuthorCreateView(AuthorRequiredMixin,CreateView):
    model = Author
    template_name = "authors/author_new.html"
    fields = "__all__"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "add author" 
        return context

class AuthorListView(AuthorRequiredMixin,ListView):
    model = Author
    template_name = "authors/author_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "all authors" 
        return context

class AuthorUpdateView(AuthorRequiredMixin,UpdateView):
    model = Author
    template_name = "authors/author_edit.html"
    fields = "__all__"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "update author" 
        return context

class AuthorDetailView(DetailView):
    model = Author
    template_name = "authors/author_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "author detail" 
        return context

class AuthorDeleteView(AuthorRequiredMixin,DeleteView):
    model = Author
    template_name = "authors/author_delete.html"
    success_url = reverse_lazy("category_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "delete author" 
        return context


def book_by_author_view(request, pk):
    context = {}
    template_name = 'authors/author_book.html'
    author = Author.objects.get(pk=pk)
    book_list = Book.objects.filter(author__pk=pk)
    context['book_list'] = book_list
    context['author'] = author
    return render(request, template_name, context)