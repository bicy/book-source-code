from django.db import models
from django.urls import reverse

# Create your models here.
class Author(models.Model):
    name = models.CharField(("author name"), max_length=80)
    biography = models.TextField(("biography"),blank=True)
    image = models.ImageField(upload_to="images/authors", blank=True)
   
    class Meta:
        verbose_name = "author"
        verbose_name_plural = "authors"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("author_detail", kwargs={"pk": self.pk})