from django.db import models
from django.urls import reverse

# Create your models here.

class Publisher(models.Model):
    publisher_name = models.CharField(("Publisher name"), max_length=80)

    class Meta:
        verbose_name = "publisher"
        verbose_name_plural = ("publishers")

    def __str__(self):
        return self.publisher_name

    def get_absolute_url(self):
        return reverse("publisher_detail", kwargs={"pk": self.pk})
