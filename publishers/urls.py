from django.urls import path

from .views import (
    PublisherCreateView,
    PublisherDeleteView,
    PublisherDetailView,
    PublisherListView,
    PublisherUpdateView)

urlpatterns = [
    path('', PublisherListView.as_view(), name="publisher_list"),
    path('<int:pk>/', PublisherDetailView.as_view(), name="publisher_detail"),
    path("<int:pk>/edit/", PublisherUpdateView.as_view(), name="publisher_edit"),
    path("<int:pk>/delete/", PublisherDeleteView.as_view(), name="publisher_delete"),
    path("new/", PublisherCreateView.as_view(), name="publisher_new")
]