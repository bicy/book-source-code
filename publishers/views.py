from django.shortcuts import render
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (ListView, UpdateView, DeleteView, DetailView, CreateView)

from .models import Publisher

class PublisherRequiredMixin(PermissionRequiredMixin, LoginRequiredMixin):
    login_url = "login"
    permission_required = ("publishers.add_publisher","publishers.change_publisher","publishers.delete_publisher")

# Create your views here.
class PublisherCreateView(PublisherRequiredMixin,CreateView):
    model = Publisher
    template_name = "publishers/publisher_new.html"
    fields = "__all__"

class PublisherListView(PublisherRequiredMixin,ListView):
    model = Publisher
    template_name = "publishers/publisher_list.html"

class PublisherDetailView(PublisherRequiredMixin,DetailView):
    model = Publisher
    template_name = "publishers/publisher_detail.html"

class PublisherUpdateView(PublisherRequiredMixin,UpdateView):
    model = Publisher 
    template_name = "publishers/publisher_edit.html"
    fields = "__all__"

class PublisherDeleteView(PublisherRequiredMixin,DeleteView):
    model = Publisher
    template_name = "publishers/publisher_delete.html"
    success_url = reverse_lazy("publisher_list")

